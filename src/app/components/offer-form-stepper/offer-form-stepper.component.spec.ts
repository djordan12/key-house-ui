import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OfferFormStepperComponent } from './offer-form-stepper.component';

describe('OfferFormStepperComponent', () => {
  let component: OfferFormStepperComponent;
  let fixture: ComponentFixture<OfferFormStepperComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OfferFormStepperComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OfferFormStepperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
