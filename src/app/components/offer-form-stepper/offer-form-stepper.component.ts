import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { OfferService } from 'src/app/services/offer.service';

@Component({
  selector: 'app-offer-form-stepper',
  templateUrl: './offer-form-stepper.component.html',
  styleUrls: ['./offer-form-stepper.component.css']
})
export class OfferFormStepperComponent implements OnInit {
  formGroup: FormGroup;

  get formArray(): AbstractControl | null { return this.formGroup.get('formArray'); }

  constructor(private _formBuilder: FormBuilder, private _offerService: OfferService) { }

  ngOnInit() {
    this.formGroup = this._formBuilder.group({
      formArray: this._formBuilder.array([
        this._formBuilder.group({
          firstNameFormCtrl: ['John', Validators.required],
          lastNameFormCtrl: ['Smith', Validators.required]
        }),
        this._formBuilder.group({
          addressFormCtrl: ['123 Yellow Brick Rd.', Validators.required],
          zipCodeFormCtrl: ['77777', Validators.required]
        }),
        this._formBuilder.group({
          emailFormCtrl: ['john.smith@yellowbrickrd.com', Validators.email],
          phoneFormCtrl: ['111-111-1111', Validators.required]
        }),
      ])
    });
  }

  public onSubmit(): void {
    const form = this.formGroup;
    console.log(form);
    this._offerService.sendOffer(
      form.value.formArray[0].firstNameFormCtrl,
      form.value.formArray[0].lastNameFormCtrl,
      form.value.formArray[1].addressFormCtrl,
      form.value.formArray[1].zipCodeFormCtrl,
      form.value.formArray[2].emailFormCtrl,
      form.value.formArray[2].phoneFormCtrl,
      'message'
    );
  }
}
