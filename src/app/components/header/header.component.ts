import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  @Output() sidenavToggle: EventEmitter<boolean>;

  constructor() {
    this.sidenavToggle = new EventEmitter();
  }

  ngOnInit() {
  }

  onToggleSidenav() {
    this.sidenavToggle.emit();
  }

  getOffer() {
    const el = document.getElementById('offer');
    el.scrollIntoView();
  }
}
