import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-sidenav-list',
  templateUrl: './sidenav-list.component.html',
  styleUrls: ['./sidenav-list.component.css']
})
export class SideNavListComponent implements OnInit {

    @Output() sidenavToggle: EventEmitter<boolean>;

    constructor() {
      this.sidenavToggle = new EventEmitter();
    }

    ngOnInit() {
    }

    onToggle() {
      this.sidenavToggle.emit();
    }
}