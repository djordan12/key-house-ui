import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  pros: string[] = [
    'No Repairs',
    'No Fees',
    'No Commissions',
    'No Contractors',
    'Close in Days'
  ];

  cons: string[] = [
    'Costly Repairs',
    'Closing Fees',
    'Multiple Showings',
    'Realtor Commission',
    'Close in Months'
  ];

  constructor() { }

  ngOnInit() {
  }

}
