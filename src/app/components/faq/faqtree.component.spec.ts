import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FAQTreeComponent } from './faqtree.component';

describe('FaqComponent', () => {
  let component: FAQTreeComponent;
  let fixture: ComponentFixture<FAQTreeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FAQTreeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FAQTreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
