import { Component, OnInit } from '@angular/core';
import { NestedTreeControl } from '@angular/cdk/tree';
import { MatTreeNestedDataSource } from '@angular/material/tree';

interface FAQNode {
  qa: string;
  children?: FAQNode[];
}

const FAQ_TREE_DATA: FAQNode[] = [
  {
    qa: 'What type of homes do you purchase?',
    children: [
      { qa: 'We buy many types of homes. From single family, to multi-units, apartments, and even land.' }
    ]
  },
  {
    qa: 'How long does it take for me to get an offer?',
    children: [
      { qa: `Typically, by just answering a few questions about your property, like the number of bedrooms, bathrooms,
             overall condition and location we can give you an offer estimate over the phone.` },
      { qa: 'We will schedule a time to come and view the property to confirm property details, and give you a formal offer on the spot..' }
    ]
  },
  {
    qa: 'Does the condition of my home matter?',
    children: [
      { qa: 'No! We buy homes “as is.”'},
      { qa: 'This means it does not matter if the house is outdated, has structural damage and or other costly repairs!'}
    ]
  },
  {
    qa: 'How much does it cost for you to see my property?',
    children: [
      { qa: 'Zero! We will come inspect your property and make you a no obligation offer.'},
    ]
  },
  {
    qa: 'Why are there no fees?',
    children: [
      { qa: `The price you've agreed to is the price you'll receive!`},
      { qa: `Sine you're working directly with the buyer we can cut out the middle men!`},
      { qa: 'Any other fees related to the closing process is paid for by us! Including any attorney or title fees.'}
    ]
  },
  {
    qa: 'How soon can I get the cash?',
    children: [
      { qa: 'As soon as you close you will receive your cash. We can close most home purchases in as little as 14 days.'},
    ]
  },
  {
    qa: `What if I don't live in the area?`,
    children: [
      { qa: 'Most of our closings are done remotely. All you need to do is meet with a notary, which we can arrange.'},
    ]
  },
];



@Component({
  selector: 'app-faqtree',
  templateUrl: './faqtree.component.html',
  styleUrls: ['./faqtree.component.css']
})
export class FAQTreeComponent implements OnInit {

  treeControl = new NestedTreeControl<FAQNode>(node => node.children);
  dataSource = new MatTreeNestedDataSource<FAQNode>();

  constructor() {
    this.dataSource.data = FAQ_TREE_DATA;
  }

  ngOnInit() {
  }

  hasChild = (_: number, node: FAQNode) => !!node.children && node.children.length > 0;
}

// What does it cost for Myers to come look at my house?
// Nothing! We will come inspect your property, and give you a no obligation offer. Unlike many other home purchasers and real estate agents there is no additional fee charged once we purchase your property.