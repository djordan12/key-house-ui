import { NgModule } from '@angular/core';
import {
    MatInputModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    MatCardModule,
    MatExpansionModule,
    MatMenuModule,
    MatCheckboxModule,
    MatTableModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSnackBarModule,
    MatListModule,
    MatSortModule,
    MatSelectModule,
    MatSidenavModule,
    MatProgressSpinnerModule,
    MatPaginatorModule,
    MatDialogModule,
    MatStepperModule,
    MatTabsModule,
    MatSliderModule,
    MatTreeModule
    } from '@angular/material';

@NgModule({
    exports: [
        MatInputModule,
        MatToolbarModule,
        MatIconModule,
        MatButtonModule,
        MatCardModule,
        MatExpansionModule,
        MatMenuModule,
        MatCheckboxModule,
        MatTableModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatSnackBarModule,
        MatListModule,
        MatSortModule,
        MatSelectModule,
        MatSidenavModule,
        MatProgressSpinnerModule,
        MatPaginatorModule,
        MatDialogModule,
        MatStepperModule,
        MatTabsModule,
        MatSliderModule,
        MatTreeModule
    ]
})

export class MaterialModule {}