import { Component } from '@angular/core';
import { IconService } from './shared/widgets/icons/icon.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'key-ui';

  constructor(private iconService: IconService) { }
  ngOnInit(): void {
    this.iconService.registerIcons();
  }
}
