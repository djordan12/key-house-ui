export enum Icons {
    Logo = 'logo',
    LogoPhrase = 'logo_phrase',
    JordanPhrase = 'jordans_logo',
}