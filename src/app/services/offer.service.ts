import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Offer } from '../models/offer.model';

@Injectable({
  providedIn: 'root'
})
export class OfferService {

  constructor(private http: HttpClient, private router: Router) { }

  public sendOffer(firstName, lastName, address, zip, email, mobile, message) {

    const offerData: Offer = {
      firstName,
      lastName,
      address,
      zip,
      email,
      mobile,
      message,
    };

    this.http.post('https://10gd832zj2.execute-api.us-east-1.amazonaws.com/dev01/offer-notification', offerData)
      .subscribe((response: any) => {
        console.log(response);
      }, error => {
        console.log(error);
      });
  }
}
