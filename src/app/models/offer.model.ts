export interface Offer {
    firstName: string;
    lastName: string;
    address: string;
    zip: string;
    email: string;
    mobile: string;
    message: string;
}
